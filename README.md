# CleanCodeStyleByBunyat

Примеры кода.

##### SessionReplacerFilter.java - фильтр с построением прокси объекта.
Класс стал необходим когда в приложение на основе spring security,  
по мимо основной его функциональности, (авторизационный сервис и сервер ресурсов) добавилась  
oauth авторизация через сервис esia (логин на госуслугах) и  
хранение авторизационных токенов в mongoDb (для шардирования).  
  
Кастомное решение oauth2 c mongo токенстором, не позволяет авторизационному сервису работать из коробки, код под монгу писали не разрабочики спринг security.

##### Пакет integration/ - настроенный для использования webClient()  
- Использует flux веб клиент из spring webflux    
- Обрабатывает полученные ошибки интеграционного вызова в красивый ответ типа ErrorDescription
```
{
    "serviceName": "kcr-core",
    "error": "Method Not Allowed",
    "message": "Integration call error [405]",
    "status": "METHOD_NOT_ALLOWED",
    "internalError": {
        "message": "Integration call error",
        "body": "<html>\r\n<head><title>405 Not Allowed</title></head>\r\n<body bgcolor=\"white\">\r\n<center><h1>405 Not Allowed</h1></center>\r\n<hr><center>nginx/1.13.8</center>\r\n</body>\r\n</html>\r\n"
    },
    "timestamp": "2020-06-23T11:05:38.7562277+03:00"
}
```
- Настроенны кастомные логи, с хедерами, телами запроса и временем ответа. Формат:  
```
2020-06-19 13:00:25.703 DEBUG 1 --- [reactor-http-epoll-1] k.i.l.LoggableClientHttpRequestDecorator : Sent request: POST http://nsi-service:5054/nsi/dictionaries/channels/records/record-list
Authorization=Bearer 99b6562a-24cb-4f8f-833e-09d5e9c77573
Content-Type=application/json
Content-Length=79

2020-06-19 13:00:25.816 DEBUG 1 --- [reactor-http-epoll-1] .i.l.LoggableClientHttpResponseDecorator : Received response(112ms): 200 OK on POST http://nsi-service:5054/nsi/dictionaries/channels/records/record-list
X-Content-Type-Options=nosniff
X-XSS-Protection=1; mode=block
Cache-Control=no-cache, no-store, max-age=0, must-revalidate
Pragma=no-cache
Expires=0
X-Frame-Options=DENY
Content-Type=application/json
Transfer-Encoding=chunked
Date=Fri, 19 Jun 2020 13:00:25 GMT
Payload: {"records":[{"recordId":"31ab22d2-96bf-11ea-adca-001a4a160479","values":{"name":"МФЦ","provisionType.ref":["31aae7ae-96bf-11ea-adca-001a4a160479"],"provisionType":"31aae7ae-96bf-11ea-adca-001a4a160479"},"name":"МФЦ"},{"recordId":"31a
b248a-96bf-11ea-adca-001a4a160479","values":{"name":"ЕПГУ","provisionType.ref":["31aae89e-96bf-11ea-adca-001a4a160479"],"defaultExtraterritorialityId":"FEDERAL","provisionType":"31aae89e-96bf-11ea-adca-001a4a160479"},"name":"ЕПГУ"}]}
```   
- Гибко настраивается в application.yml  
```
integration:
  # Основная конфигруация
  headers-to-pass-through:
    - Authorization
    - XXX-Custom-Header
  auth-enabled: false
  # Конфигурации интеграционных сервисов
  auth:
    client-id: server_app
    client-secret: secret
    access-token-url: http://localhost:5000
    grant-type: client_credentials
    scope: server
  user:
    all-users-url: http://localhost:5000
  dictionaries:
    url: http://localhost:5054
    get-all-records-url: http://localhost:5054

```  
