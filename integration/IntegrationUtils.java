import lombok.experimental.UtilityClass;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import config.ErrorDescriptionExceptionHandler;
import exceptions.ErrorDescription;
import exceptions.IntegrationException;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Утильный класс, который позволяет обработать стандартную HTTP ошибку спринга.
 * - Превращает http ответ в {@link ErrorDescription}, который выбросится после отработки метода block() у {@link WebClient}.
 * - Обработка ошибки происходит в {@link ErrorDescriptionExceptionHandler}
 */
@UtilityClass
public class IntegrationUtils {
    public static Mono<ClientResponse> handleError(ClientResponse response) {
        if (response.statusCode().isError()) {
            final boolean isJson = response.headers()
                    .contentType()
                    .map(mediaType -> mediaType.equals(MediaType.APPLICATION_JSON) || mediaType.equals(MediaType.APPLICATION_JSON_UTF8))
                    .orElse(false);

            if (isJson) {
                return response
                        .bodyToMono(new ParameterizedTypeReference<Map<String, String>>() {
                        })
                        .map(map -> mapErrorDescription(response, map))
                        .flatMap(errorDescription -> Mono.error(new IntegrationException(errorDescription)));
            } else {
                return response
                        .bodyToMono(new ParameterizedTypeReference<String>() {
                        })
                        .map(s -> {
                            Map<String, String> map = new HashMap<>();
                            map.put("message", "Integration call error");
                            map.put("body", s);
                            return mapErrorDescription(response, map);
                        })
                        .flatMap(errorDescription -> Mono.error(new IntegrationException(errorDescription)));
            }
        }

        return Mono.just(response);
    }

    private static ErrorDescription mapErrorDescription(ClientResponse response, Map<String, String> map) {
        final String errorCode =
                Optional.ofNullable(map.get("error"))
                        .orElse(response.statusCode().getReasonPhrase());

        final String userMessage =
                Optional.ofNullable(map.get("message"))
                        .orElseGet(() ->
                                Optional.ofNullable(map.get("error"))
                                        .orElse(response.statusCode().getReasonPhrase()))
                        + " ["
                        + response.statusCode().value()
                        + ']';

        return ErrorDescription.builder()
                .serviceName("kcr-core")
                .error(errorCode)
                .message(userMessage)
                .status(response.statusCode())
                .timestamp(ZonedDateTime.now())
                .internalError(map)
                .build();
    }
}
