import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AuthTokenDto {
    private static final int ACCURACY = 60 * 1000;

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("expires_in")
    private Long expiresIn; // Token life time in seconds

    private Long creationTimestamp = System.currentTimeMillis();

    /**
     * Expiration date == current date + TokenLifeTime - 1 minute as accuracy
     */
    public boolean isExpired() {
        return System.currentTimeMillis() > creationTimestamp + expiresIn * 1000 - ACCURACY;
    }
}
