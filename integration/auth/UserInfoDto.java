import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public
class UserInfoDto {
    private String id;
    private String surname;
    private String name;
    private String middleName;
    private Boolean isDeleted;
    private String position;
    private String defaultDepartmentId;
    private List<UserDepartment> departmentList;
}
