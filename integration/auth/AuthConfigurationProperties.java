import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("integration.auth")
public class AuthConfigurationProperties {
    private String clientId;
    private String clientSecret;
    private String accessTokenUrl;
    private String grantType;
    private String scope;
}
