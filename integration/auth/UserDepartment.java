import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public
class UserDepartment {
    private String id;
    private String fullName;
    private String ogrn;
    private String code;
}
