/**
 * Интеграционный сервис для получение токена у авторизационного сервера.
 */
public interface AuthIntegrationService {
    /**
     * Авторизация по серверному scope
     *
     * @return Авторизационный токен
     */
    AuthTokenDto serverAuth();
}
