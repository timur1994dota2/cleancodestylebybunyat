import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import exceptions.AuthenticationException;
import exceptions.ErrorDescription;
import integration.HttpService;
import integration.IntegrationConfigurationProperties;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Стандартно настроенный http клиент для получение токена в authService
 */
@Slf4j
@Component
public class AuthIntegrationServiceImpl extends HttpService<AuthConfigurationProperties> implements AuthIntegrationService {

    @Value("${spring.application.name}")
    private String applicationName;

    public AuthIntegrationServiceImpl(AuthConfigurationProperties configuration,
                                      WebClient.Builder builder,
                                      IntegrationConfigurationProperties integrationConfigurationProperties) {
        super(builder, configuration, integrationConfigurationProperties);
    }

    @Override
    public AuthTokenDto serverAuth() {
        return getClient()
                .post()
                .uri(configuration.getAccessTokenUrl())
                .body(BodyInserters.fromFormData("grant_type", configuration.getGrantType())
                        .with("scope", configuration.getScope())
                        .with("client_id", configuration.getClientId())
                        .with("client_secret", configuration.getClientSecret()))
                .exchange()
                .flatMap(this::handleGetTokenError)
                .flatMap(response -> response.bodyToMono(AuthTokenDto.class))
                .block();
    }

    private Mono<ClientResponse> handleGetTokenError(ClientResponse response) {
        if (response.statusCode().isError()) {
            return response
                    .bodyToMono(AuthError.class)
                    .map(authError -> mapErrorDescription(response, authError))
                    .flatMap(errorDescription -> Mono.error(new AuthenticationException(errorDescription)));
        }

        return Mono.just(response);
    }

    private ErrorDescription mapErrorDescription(ClientResponse response, AuthError authError) {
        final String errorCode =
                Optional.ofNullable(authError.error)
                        .orElse(response.statusCode().getReasonPhrase());

        final String userMessage =
                Optional.ofNullable(authError.errorDescription)
                        .orElseGet(() ->
                                Optional.ofNullable(authError.error)
                                        .orElse(response.statusCode().getReasonPhrase()))
                        + " ["
                        + response.statusCode().value()
                        + ']';

        return ErrorDescription.builder()
                .serviceName(applicationName)
                .error(errorCode)
                .message(userMessage)
                .status(response.statusCode())
                .timestamp(ZonedDateTime.now())
                .build();
    }

    @Data
    private static class AuthError {
        @JsonProperty("error_description")
        private String errorDescription;

        private String error;
    }
}
