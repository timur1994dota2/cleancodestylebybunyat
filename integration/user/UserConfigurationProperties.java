import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("integration.user")
public class UserConfigurationProperties {
    private String allUsersUrl;
}
