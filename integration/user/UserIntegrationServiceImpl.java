import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import integration.IntegrationConfigurationProperties;
import integration.IntegrationUtils;
import integration.SingleSignOnService;
import integration.auth.AuthIntegrationService;
import integration.auth.UserInfoDto;
import models.dto.PageDto;

@Slf4j
@Component
public class UserIntegrationServiceImpl extends SingleSignOnService<UserConfigurationProperties> implements UserIntegrationService {

    public UserIntegrationServiceImpl(AuthIntegrationService authIntegrationService,
                                      UserConfigurationProperties configuration,
                                      WebClient.Builder builder,
                                      IntegrationConfigurationProperties integrationConfigurationProperties) {
        super(authIntegrationService, configuration, builder, integrationConfigurationProperties);
    }

    @Override
    public Page<UserInfoDto> getAllUsers() {
        return getClient()
                .get()
                .uri(configuration.getAllUsersUrl())
                .exchange()
                .flatMap(IntegrationUtils::handleError)
                .flatMap(response -> response.bodyToMono(new ParameterizedTypeReference<PageDto<UserInfoDto>>() {
                }))
                .block();
    }
}
