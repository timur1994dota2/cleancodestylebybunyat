import org.springframework.data.domain.Page;
import integration.auth.UserInfoDto;

/**
 * Интеграционный сервис для работы с пользователями.
 */
public interface UserIntegrationService {

    /**
     * Список всех пользователей из системы auth-service
     *
     * @return Все пользователи
     */
    Page<UserInfoDto> getAllUsers();
}
