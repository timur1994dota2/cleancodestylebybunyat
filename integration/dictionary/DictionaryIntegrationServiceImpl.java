import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.WebClient;
import exceptions.NoDictionaryRecordsReturnException;
import integration.IntegrationConfigurationProperties;
import integration.IntegrationUtils;
import integration.SingleSignOnService;
import integration.auth.AuthIntegrationService;
import models.dto.PageDto;
import models.dto.dictionaries.ShortDictionaryRecordDto;
import models.dto.dictionaries.ShortDictionaryRecords;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DictionaryIntegrationServiceImpl extends SingleSignOnService<DictionaryConfigurationProperties> implements DictionaryIntegrationService {

    public DictionaryIntegrationServiceImpl(AuthIntegrationService authIntegrationService,
                                            DictionaryConfigurationProperties configuration,
                                            WebClient.Builder builder,
                                            IntegrationConfigurationProperties integrationConfigurationProperties) {
        super(authIntegrationService, configuration, builder, integrationConfigurationProperties);
    }

    @Override
    public ShortDictionaryRecords getSeveralValuesByFilter(String dictionaryCode, Collection<UUID> ids) {
        return getClient()
                .post()
                .uri(configuration.getUrl(), dictionaryCode)
                .bodyValue(ids)
                .exchange()
                .flatMap(IntegrationUtils::handleError)
                .flatMap(response -> response.bodyToMono(new ParameterizedTypeReference<ShortDictionaryRecords>() {
                }))
                .block();
    }

    @Override
    public Map<UUID, String> fillDictionaryMap(String dictionaryCode, Collection<UUID> ids) {
        if (CollectionUtils.isEmpty(ids)) return Collections.emptyMap();

        return Optional.ofNullable(getClient()
                .post()
                .uri(configuration.getUrl(), dictionaryCode)
                .bodyValue(ids)
                .exchange()
                .flatMap(IntegrationUtils::handleError)
                .flatMap(response -> response.bodyToMono(new ParameterizedTypeReference<ShortDictionaryRecords>() {
                }))
                .block())
                .orElseThrow(() -> new NoDictionaryRecordsReturnException(dictionaryCode))
                .getRecords()
                .stream()
                .collect(Collectors.toMap(ShortDictionaryRecordDto::getRecordId, ShortDictionaryRecordDto::getName));
    }

    @Override
    public String getSingleValueByFilter(String dictionaryCode, UUID id) {
        final ShortDictionaryRecords block = getClient()
                .post()
                .uri(configuration.getUrl(), dictionaryCode)
                .bodyValue(List.of(id))
                .exchange()
                .flatMap(IntegrationUtils::handleError)
                .flatMap(response -> response.bodyToMono(new ParameterizedTypeReference<ShortDictionaryRecords>() {
                }))
                .block();

        return block == null
                ? ""
                : block.getRecords().stream().findFirst().map(ShortDictionaryRecordDto::getName).orElse("");
    }

    @Override
    public ShortDictionaryRecordDto getSingleRecordByFilter(String dictionaryCode, UUID id) {
        final ShortDictionaryRecords block = getClient()
                .post()
                .uri(configuration.getUrl(), dictionaryCode)
                .bodyValue(List.of(id))
                .exchange()
                .flatMap(IntegrationUtils::handleError)
                .flatMap(response -> response.bodyToMono(new ParameterizedTypeReference<ShortDictionaryRecords>() {
                }))
                .block();

        return block == null
                ? null
                : block.getRecords().stream().findFirst().orElse(null);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public List<ShortDictionaryRecordDto> getAllRecords(String dictionaryCode) {
        return getClient()
                .post()
                .uri(configuration.getGetAllRecordsUrl(), dictionaryCode)
                .bodyValue("{}")
                .exchange()
                .flatMap(IntegrationUtils::handleError)
                .flatMap(response -> response.bodyToMono(new ParameterizedTypeReference<PageDto<ShortDictionaryRecordDto>>() {
                }))
                .block()
                .getContent();
    }
}
