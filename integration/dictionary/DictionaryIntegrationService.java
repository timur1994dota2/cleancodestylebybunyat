import models.dto.PageDto;
import models.dto.dictionaries.ShortDictionaryRecordDto;
import models.dto.dictionaries.ShortDictionaryRecords;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Http сервис для работы с сервисом справочников
 */
public interface DictionaryIntegrationService {
    /**
     * Метод возвращает список значений справочников
     *
     * @param dictionaryCode Строковый идентификатор справочника
     * @param ids IDs записей
     * @return Содержимое справочника соответственно переданным Ids
     */
    ShortDictionaryRecords getSeveralValuesByFilter(String dictionaryCode, Collection<UUID> ids);

    /**
     * Метод возвращает список значений справочников
     *
     * @param dictionaryCode Строковый идентификатор справочника
     * @param ids IDs записей
     * @return Содержимое справочника соответственно переданным Ids
     */
    Map<UUID, String> fillDictionaryMap(String dictionaryCode, Collection<UUID> ids);

    /**
     * Метод возвращает значение справочника
     *
     * @param dictionaryCode Строковый идентификатор справочника
     * @param id ID записи
     * @return Значение справочника для одной записи
     */
    String getSingleValueByFilter(String dictionaryCode, UUID id);

    /**
     * Метод возвращает все значения справочника
     *
     * @param dictionaryCode Строковый идентификатор справочника
     * @param id ID записи
     * @return Множества значений справочника для одной записи
     */
    ShortDictionaryRecordDto getSingleRecordByFilter(String dictionaryCode, UUID id);

    /**
     * Получение списка всех значений справочника
     *
     * @param dictionaryCode ID справочника
     * @return Все значения справочника
     */
    List<ShortDictionaryRecordDto> getAllRecords(String dictionaryCode);
}
