import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("integration.dictionaries")
public class DictionaryConfigurationProperties {
    private String url;
    private String getAllRecordsUrl;
}
