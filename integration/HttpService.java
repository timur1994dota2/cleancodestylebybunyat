import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import log.LoggableReactorClientHttpConnector;

import javax.annotation.PostConstruct;

import static org.springframework.web.reactive.function.client.WebClient.Builder;

/**
 * Абстрактный класс, наследники которого могут ходить по http используя {@link WebClient}
 * В данном классе настраиваются основные для всего приложения параметры.
 * - Прокидывание хедеров {@link HeaderContextFilter}
 * - Кастомное логирование {@link LoggableReactorClientHttpConnector}
 *
 * @param <T> класс конфигурации аннотированный {@link org.springframework.boot.context.properties.ConfigurationProperties}
 */
@Data
@Slf4j
public abstract class HttpService<T> {
    public static final ThreadLocal<HttpHeaders> HEADERS_TO_PASS_THROUGH = new ThreadLocal<>();

    private Builder builder;

    private WebClient client;

    protected final T configuration;

    private final IntegrationConfigurationProperties integrationConfigurationProperties;

    public HttpService(Builder builder,
                       T configuration,
                       IntegrationConfigurationProperties integrationConfigurationProperties) {
        this.builder = builder;
        this.configuration = configuration;
        this.integrationConfigurationProperties = integrationConfigurationProperties;
        initBuilder(this.builder);
    }

    public void initBuilder(Builder builder) {
        builder.filter(this::headersToPassFiler);
        builder.clientConnector(new LoggableReactorClientHttpConnector()); //Кастомные логи
    }

    /**
     * Действия после DI
     */
    @PostConstruct
    public void init() {
        if (builder == null) {
            throw new IllegalStateException("Already initialized");
        }

        client = builder.build();

        builder = null;
    }

    @NonNull
    protected Mono<ClientResponse> headersToPassFiler(ClientRequest request, ExchangeFunction next) {
        return next.exchange(ClientRequest.from(request)
                .headers(httpHeaders -> httpHeaders.addAll(HEADERS_TO_PASS_THROUGH.get()))
                .build());
    }
}
