import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Фильтр для прокидывание хедеров в рамках одного реквеста \ потока
 */
@Component
@RequiredArgsConstructor
@javax.servlet.annotation.WebFilter
public class HeaderContextFilter extends OncePerRequestFilter {

    private final IntegrationConfigurationProperties integrationConfigurationProperties;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain filterChain) throws ServletException, IOException {
        try {
            final HttpHeaders httpHeaders = new HttpHeaders();

            integrationConfigurationProperties.getHeadersToPassThrough()
                    .forEach(name -> {
                        final String header = request.getHeader(name);
                        if (!StringUtils.isEmpty(header)) {
                            httpHeaders.set(name, header);
                        }
                    });

            HEADERS_TO_PASS_THROUGH.set(httpHeaders);
        } finally {
            filterChain.doFilter(request, response);
        }
    }
}
