import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.http.client.reactive.ClientHttpResponse;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.lang.NonNull;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

/**
 * Декоратор ReactorClientHttpConnector для логов
 */
@Slf4j
public class LoggableReactorClientHttpConnector extends ReactorClientHttpConnector {

    @NonNull
    @Override
    public Mono<ClientHttpResponse> connect(
            @NonNull HttpMethod method,
            URI uri,
            @NonNull Function<? super ClientHttpRequest, Mono<Void>> requestCallback) {
        log.debug("Start sending request: {} {}", method, uri);
        // Атомики, так как большие скорости запросов могут привести к невалидному состоянию объекта
        final AtomicReference<LoggableClientHttpRequestDecorator> requestRef = new AtomicReference<>();
        final AtomicLong requestStartTime = new AtomicLong(System.currentTimeMillis());
        // Декорируем реквест, после выполнения, подменяем респонз декоратором
        return super.connect(
                method,
                uri,
                clientHttpRequest -> {
                    requestRef.set(
                            new LoggableClientHttpRequestDecorator(
                                    clientHttpRequest, requestStartTime));
                    return requestCallback.apply(requestRef.get());
                })
                .map(response ->
                        decorateResponse(response, requestRef.get(), requestStartTime))
                .doOnError(throwable ->
                        log.error("Cannot send request: {} {}, exception {}", method, uri, throwable.toString()));
    }

    /**
     * Метод для даункастинга LoggableClientHttpResponseDecorator в ClientHttpResponse
     */
    private ClientHttpResponse decorateResponse(
            ClientHttpResponse response,
            ClientHttpRequest request,
            AtomicLong requestStartTime) {
        return new LoggableClientHttpResponseDecorator(response, request, requestStartTime);
    }
}
