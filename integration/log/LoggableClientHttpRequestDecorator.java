import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.http.client.reactive.ClientHttpRequestDecorator;
import org.springframework.lang.NonNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Декоратор ClientHttpRequest для логирования
 */
@Slf4j
public class LoggableClientHttpRequestDecorator extends ClientHttpRequestDecorator {

    private final DataBufferLoggingSupport loggingSupport;
    private final AtomicLong requestStartTime;

    LoggableClientHttpRequestDecorator(
            ClientHttpRequest delegate, AtomicLong requestStartTime) {
        super(delegate);
        this.loggingSupport = new DataBufferLoggingSupport(log.isDebugEnabled());
        this.requestStartTime = requestStartTime;
    }

    @NonNull
    @Override
    public Mono<Void> writeWith(@NonNull Publisher<? extends DataBuffer> body) {
        return super.writeWith(Flux.from(body)
                .doOnNext(dataBuffer -> {
                    if (isReadableContentType()) {
                        loggingSupport.appendDataToBuffer(dataBuffer);
                    }
                })
                .doOnComplete(this::doLogOnComplete));
    }

    @NonNull
    @Override
    public Mono<Void> writeAndFlushWith(@NonNull Publisher<? extends Publisher<? extends DataBuffer>> publisher) {
        return super.writeAndFlushWith(Flux.from(publisher)
                .map(p -> Flux.from(p)
                        .doOnNext(dataBuffer -> {
                            if (isReadableContentType()) {
                                loggingSupport.appendDataToBuffer(dataBuffer);
                            }
                        })
                        .doOnComplete(this::doLogOnComplete)));
    }

    @NonNull
    @Override
    public Mono<Void> setComplete() {
        doLogOnComplete();
        return super.setComplete();
    }

    private boolean isReadableContentType(){
        final MediaType contentType = getDelegate().getHeaders().getContentType();
        return contentType != null
                && (contentType.toString().contains("application/json")
                || contentType.toString().contains("application/x-www-form-urlencoded"));
    }

    private void doLogOnComplete() { //todo вызывается 2 раза
        // добавляются 2 таски в io.netty.util.concurrent.SingleThreadEventExecutor.offerTask
        // одна из них в ассинхронном режиме
        log.debug(RequestLoggingUtils.generateClientHttpRequestLogMessage(
                "Sent request: ", getDelegate(), loggingSupport.getBodyAsByteArray()));
        requestStartTime.set(System.currentTimeMillis());
    }
}
