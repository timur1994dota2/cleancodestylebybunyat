import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;

/**
 * Утилитный класс для обработки {@link DataBuffer} в массив байтов
 */
public class DataBufferLoggingSupport {
    private static final DataBufferFactory bufferFactory = new DefaultDataBufferFactory();
    private final boolean logEnabled;
    private final DataBuffer buffer;

    public DataBufferLoggingSupport(boolean logEnabled) {
        this.logEnabled = logEnabled;
        buffer = bufferFactory.allocateBuffer();
    }

    /**
     * Добавляем к buffer тело запроса\ответа, что бы потом его вычитать.
     *
     * @param dataBuffer Тело запроса
     */
    public void appendDataToBuffer(DataBuffer dataBuffer) {
        if (!logEnabled) {
            return;
        }
        buffer.write(dataBuffer);
    }

    /**
     * Читаем тело запроса\ответа из буфера.
     */
    public byte[] getBodyAsByteArray() {
        final byte[] payload = new byte[buffer.readableByteCount()];
        buffer.read(payload);
        return payload;
    }
}
