import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.http.client.reactive.ClientHttpResponse;
import org.springframework.http.client.reactive.ClientHttpResponseDecorator;
import org.springframework.lang.NonNull;
import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Декоратор ClientHttpResponse для логирования
 */
@Slf4j
public class LoggableClientHttpResponseDecorator extends ClientHttpResponseDecorator {
    private final ClientHttpRequest request;
    private final DataBufferLoggingSupport loggingSupport;
    private final AtomicLong requestStartTime;

    LoggableClientHttpResponseDecorator(
            ClientHttpResponse delegate,
            ClientHttpRequest request,
            AtomicLong requestStartTime) {
        super(delegate);
        this.request = request;
        this.loggingSupport = new DataBufferLoggingSupport(log.isDebugEnabled());
        this.requestStartTime = requestStartTime;
    }

    @NonNull
    @Override
    public Flux<DataBuffer> getBody() {
        final MediaType contentType = getDelegate().getHeaders().getContentType();
        final boolean readableContentType = contentType != null
                && (contentType.toString().contains("application/json")
                || contentType.toString().contains("application/x-www-form-urlencoded"));

        return super.getBody()
                .doOnNext(dataBuffer -> {
                    if (readableContentType) {
                        loggingSupport.appendDataToBuffer(dataBuffer);
                    }
                })
                .doOnComplete(this::doLogOnComplete);
    }

    private void doLogOnComplete() {
        log.debug(RequestLoggingUtils.generateClientHttpResponseLogMessage(
                "Received response",
                requestStartTime.get(),
                request,
                getDelegate(),
                loggingSupport.getBodyAsByteArray()));
    }
}
