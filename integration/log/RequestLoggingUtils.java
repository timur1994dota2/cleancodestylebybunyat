import lombok.experimental.UtilityClass;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.http.client.reactive.ClientHttpResponse;
import org.springframework.util.MimeType;

import java.nio.charset.Charset;
import java.util.Optional;

@UtilityClass
public class RequestLoggingUtils {
    public static final String LINE_SEPARATOR = System.lineSeparator();

    public static String generateClientHttpResponseLogMessage(
            String prefix,
            long requestStartTime,
            ClientHttpRequest request,
            ClientHttpResponse response,
            byte[] payload) {
        final StringBuilder msg = new StringBuilder();
        final HttpStatus status = response.getStatusCode();
        msg.append(prefix)
                .append("(")
                .append(System.currentTimeMillis() - requestStartTime)
                .append("ms): ")
                .append(status.value())
                .append(" ")
                .append(status.getReasonPhrase())
                .append(" on ")
                .append(request.getMethod())
                .append(" ")
                .append(request.getURI())
                .append(LINE_SEPARATOR);

        addHeadersAndBody(msg, response.getHeaders(), payload);
        return msg.toString();
    }

    public static String generateClientHttpRequestLogMessage(
            String prefix, ClientHttpRequest request, byte[] payload) {
        final StringBuilder msg = new StringBuilder();
        msg.append(prefix)
                .append(request.getMethod())
                .append(" ")
                .append(request.getURI())
                .append(LINE_SEPARATOR);

        addHeadersAndBody(msg, request.getHeaders(), payload);
        return msg.toString();
    }

    private static void addHeadersAndBody(
            StringBuilder msg, HttpHeaders headers, byte[] payload) {
        headers.forEach((headerName, headerValues) -> {
            if (headerValues.isEmpty()) {
                msg.append(headerName).append("=").append(LINE_SEPARATOR);
            }
            headerValues.forEach(headerValue ->
                    msg.append(headerName).append("=").append(headerValue).append(LINE_SEPARATOR));
        });

        if (payload != null && payload.length > 0) {
            final MediaType contentType = headers.getContentType();
            final Charset charset = Optional.ofNullable(contentType)
                    .map(MimeType::getCharset)
                    .orElse(Charset.defaultCharset());
            msg.append("Payload: ").append(new String(payload, charset));
        }
    }
}
