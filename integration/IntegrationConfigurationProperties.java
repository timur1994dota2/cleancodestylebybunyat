import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collections;
import java.util.List;

@Data
@ConfigurationProperties("integration")
public class IntegrationConfigurationProperties {
    private List<String> headersToPassThrough = Collections.emptyList();
    private boolean authEnabled;
}
