import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import integration.auth.AuthIntegrationService;
import integration.auth.AuthTokenDto;

/**
 * Абстрактный класс, наследники которого при веб запросе имеют авторизационный токен.
 * - Использует {@link AuthIntegrationService} для авторизационного запроса.
 * - В веб клиент проставляется фильтр который устанавливает хедер {@link HttpHeaders#AUTHORIZATION}. Токен кешируется.
 *
 * @param <T> класс конфигурации аннотированный {@link org.springframework.boot.context.properties.ConfigurationProperties}
 */
@Slf4j
public abstract class SingleSignOnService<T> extends HttpService<T> {
    private AuthTokenDto token;
    private final AuthIntegrationService authIntegrationService;

    public SingleSignOnService(
            AuthIntegrationService authIntegrationService,
            T configuration,
            WebClient.Builder builder,
            IntegrationConfigurationProperties integrationConfigurationProperties) {
        super(builder, configuration, integrationConfigurationProperties);
        this.authIntegrationService = authIntegrationService;
    }

    @Override
    public void initBuilder(WebClient.Builder builder) {
        super.initBuilder(builder);
        if (getIntegrationConfigurationProperties().isAuthEnabled()) {
            builder.filter(this::authTokenFilter);
        }
    }

    @NonNull
    protected Mono<ClientResponse> authTokenFilter(ClientRequest request, ExchangeFunction next) {
        if (token != null && !token.isExpired()) {
            log.debug("Cache token already exist {}", token.getAccessToken());
            return next.exchange(ClientRequest.from(request)
                    .header(HttpHeaders.AUTHORIZATION, "Bearer" + token.getAccessToken())
                    .build());
        }

        log.debug("Will authenticated request");
        token = authIntegrationService.serverAuth();
        return next.exchange(ClientRequest.from(request)
                .header(HttpHeaders.AUTHORIZATION, "Bearer" + token.getAccessToken())
                .build());
    }
}
