import lombok.RequiredArgsConstructor;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import org.springframework.session.data.mongo.MongoOperationsSessionRepository;
import org.springframework.session.data.mongo.MongoSession;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.springframework.session.web.http.SessionRepositoryFilter.SESSION_REPOSITORY_ATTR;

/**
 * Фильтр, который обучает спринг использовать правильную монго сессию в не зависимости от потока исполнения
 * Класс необходим для правильной работы внедренной зависимости httpSession, которая используется в классах
 * {@link UserOrganizationController},
 * {@link EsiaOAuth2Utils},
 * {@link EsiaAuthorizationCodeAccessTokenProvider},
 * {@link EsiaUserInfoTokenServices}.
 * <p> Как работает внедрение сессии в спринге:
 * Поле в классы спринг внедряет при старте приложения, однако информация о сессии появляется позже.
 * Спринг использует proxyFactory, смотри {@link WebApplicationContextUtils} SessionObjectFactory#getObject()
 * который внедряет httpSession прокси объектом взятым из ThreadLocal переменной.
 * <p> При смене потока текущее значение внедренной зависимости HttpSession теряется.
 * <p> Этот фильтр решает проблему разных сессий в разных потоках.
 */
@Service
@RequiredArgsConstructor
public class SessionReplacerFilter extends OncePerRequestFilter {

    private final MongoOperationsSessionRepository sessionRepository;

    public static final String CURRENT_MONGO_SESSION = SESSION_REPOSITORY_ATTR + ".CURRENT_SESSION";

    /**
     * Метод ищет сессию созданную {@link org.springframework.session.web.http.SessionRepositoryFilter},
     * которая находится в атрибутах реквеста по ключу {@link SessionReplacerFilter#CURRENT_MONGO_SESSION}.
     * <p> Если сессия найдена создается прокси объект, с переопределенным методом getSession(),
     * который помещается в ThreadLocal переменную, из которой спринг внедряет зависимость HttpSession в поле.
     * {@link WebApplicationContextUtils} SessionObjectFactory#getObject().
     * <p> Так же метод создает прокси на HttpSession, переопределяя вызовы getAttribute() и setAttribute(),
     * получая всегда актуальную информацию из {@link MongoOperationsSessionRepository}
     */
    public void replaceSpringProxySessionFactory() {
        final ServletRequestAttributes requestAttributes = currentRequestAttributes();
        final HttpServletRequest servletRequest = requestAttributes.getRequest();
        final Object mongoSession = servletRequest.getAttribute(CURRENT_MONGO_SESSION);

        if (mongoSession == null) {
            return;
        }

        //создаем прокси с помощью cglib на метод получения сессии.
        final Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(HttpServletRequest.class);
        enhancer.setUseFactory(false);
        enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
            if ("getSession".equals(method.getName()) && args.length == 0) {
                return createProxyMongoSession(mongoSession);
            } else {
                return method.invoke(servletRequest, args);
            }
        });
        final HttpServletRequest o = (HttpServletRequest) enhancer.create();
        final ServletRequestAttributes result = new ServletRequestAttributes(o, requestAttributes.getResponse());
        RequestContextHolder.setRequestAttributes(result);
    }

    /**
     * Метод создает прокси обхект HttpSession у которого подменяются методы getAttribute и setAttribute
     *
     * @param currentSessionRequestAttribute Атрибут запроса, который содержит в себе объект сессии
     * @return Прокси объект или неизмененный объект, если он не является инстансом {@link HttpSession}
     */
    public Object createProxyMongoSession(Object currentSessionRequestAttribute) {
        if (currentSessionRequestAttribute instanceof HttpSession) {
            final HttpSession mongoSession = (HttpSession) currentSessionRequestAttribute;

            final Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(HttpServletRequest.class);
            enhancer.setUseFactory(false);
            enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
                if ("getAttribute".equals(method.getName()) && args.length == 1) {
                    final String id = mongoSession.getId();
                    final MongoSession persistedSession = sessionRepository.findById(id);
                    final String arg = (String) args[0];
                    return persistedSession.getAttribute(arg);
                } else if ("setAttribute".equals(method.getName()) && args.length == 2) {
                    final String id = mongoSession.getId();
                    final MongoSession persistedSession = sessionRepository.findById(id);
                    final String key = (String) args[0];
                    final Object value = args[1];
                    persistedSession.setAttribute(key, value);
                    sessionRepository.save(persistedSession);
                    return null; // возвращаем null т.к. в сигнатуре метода void
                } else {
                    return method.invoke(currentSessionRequestAttribute, args);
                }
            });

            return mongoSession;
        }
        return currentSessionRequestAttribute;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            replaceSpringProxySessionFactory();
        } finally {
            filterChain.doFilter(request, response);
        }
    }

    private static ServletRequestAttributes currentRequestAttributes() {
        final RequestAttributes requestAttr = RequestContextHolder.currentRequestAttributes();
        if (!(requestAttr instanceof ServletRequestAttributes)) {
            throw new IllegalStateException("Current request is not a servlet request");
        }
        return (ServletRequestAttributes) requestAttr;
    }
}
